QUnit.test('Testing the new add function with four sets of inputs', function (assert) {
    assert.throws(function  (){myFunc(65);}, 1, "Tested with positive number");
    assert.throws(function (){myFunc(130);}, 2, "Tested with Anotherpositive number");
    assert.throws(function (){myFunc(-65);}, null, "Tested with negative number");
    assert.throws(function (){myFunc("hi");}, null, "Tested with String Argument");
});